require 'spec_helper'
require 'parslet/rig/rspec'

describe Xsampa::Parser do
  subject { described_class.new }

  describe 'symbols' do
    it 'parses syllable breaks' do
      expect(subject).to parse('.').as(other_symbol: '.')
    end

    it 'parses primary stresses' do
      expect(subject).to parse('"').as(other_symbol: '"')
    end

    it 'parses secondary stresses' do
      expect(subject).to parse('%').as(other_symbol: '%')
    end

    it 'parses palatalizations' do
      expect(subject).to parse('\'').as(other_symbol: '\'')
    end

    it 'parses longs' do
      expect(subject).to parse(':').as(other_symbol: ':')
    end

    it 'parses half longs' do
      expect(subject).to parse(':\\').as(other_symbol: ':\\')
    end

    it 'parses separators' do
      expect(subject).to parse('-').as(other_symbol: '-')
    end

    it 'parses escapes' do
      expect(subject).to parse('*').as(other_symbol: '*')
    end

    it 'parses indeterminacies' do
      expect(subject).to parse('/').as(other_symbol: '/')
    end

    it 'parses nonsegmental starts' do
      expect(subject).to parse('<').as(other_symbol: '<')
    end

    it 'parses nonsegmental ends' do
      expect(subject).to parse('>').as(other_symbol: '>')
    end

    it 'parses upsteps' do
      expect(subject).to parse('^').as(other_symbol: '^')
    end

    it 'parses downsteps' do
      expect(subject).to parse('!').as(other_symbol: '!')
    end

    it 'parses minor groups' do
      expect(subject).to parse('|').as(other_symbol: '|')
    end

    it 'parses major groups' do
      expect(subject).to parse('||').as(other_symbol: '||')
    end

    it 'parses linking marks' do
      expect(subject).to parse('-\\').as(other_symbol: '-\\')
    end
  end

  describe 'vowels' do
    it 'parses a' do
      expect(subject).to parse('a').as(vowel: 'a')
    end

    it 'parses e' do
      expect(subject).to parse('e').as(vowel: 'e')
    end

    it 'parses i' do
      expect(subject).to parse('i').as(vowel: 'i')
    end

    it 'parses o' do
      expect(subject).to parse('o').as(vowel: 'o')
    end

    it 'parses u' do
      expect(subject).to parse('u').as(vowel: 'u')
    end

    it 'parses y' do
      expect(subject).to parse('y').as(vowel: 'y')
    end

    it 'parses A' do
      expect(subject).to parse('A').as(vowel: 'A')
    end

    it 'parses E' do
      expect(subject).to parse('E').as(vowel: 'E')
    end

    it 'parses I' do
      expect(subject).to parse('I').as(vowel: 'I')
    end

    it 'parses I\\' do
      expect(subject).to parse('I\\').as(vowel: 'I\\')
    end

    it 'parses M' do
      expect(subject).to parse('M').as(vowel: 'M')
    end

    it 'parses O' do
      expect(subject).to parse('O').as(vowel: 'O')
    end

    it 'parses Q' do
      expect(subject).to parse('Q').as(vowel: 'Q')
    end

    it 'parses U' do
      expect(subject).to parse('U').as(vowel: 'U')
    end

    it 'parses U\\' do
      expect(subject).to parse('U\\').as(vowel: 'U\\')
    end

    it 'parses V' do
      expect(subject).to parse('V').as(vowel: 'V')
    end

    it 'parses Y' do
      expect(subject).to parse('Y').as(vowel: 'Y')
    end

    it 'parses @' do
      expect(subject).to parse('@').as(vowel: '@')
    end

    it 'parses @\\' do
      expect(subject).to parse('@\\').as(vowel: '@\\')
    end

    it 'parses {' do
      expect(subject).to parse('{').as(vowel: '{')
    end

    it 'parses }' do
      expect(subject).to parse('}').as(vowel: '}')
    end

    it 'parses 1' do
      expect(subject).to parse('1').as(vowel: '1')
    end

    it 'parses 2' do
      expect(subject).to parse('2').as(vowel: '2')
    end

    it 'parses 3' do
      expect(subject).to parse('3').as(vowel: '3')
    end

    it 'parses 3\\' do
      expect(subject).to parse('3\\').as(vowel: '3\\')
    end

    it 'parses 6' do
      expect(subject).to parse('6').as(vowel: '6')
    end

    it 'parses 7' do
      expect(subject).to parse('7').as(vowel: '7')
    end

    it 'parses 8' do
      expect(subject).to parse('8').as(vowel: '8')
    end

    it 'parses 9' do
      expect(subject).to parse('9').as(vowel: '9')
    end

    it 'parses &' do
      expect(subject).to parse('&').as(vowel: '&')
    end
  end

  describe 'consonants' do
    it 'parses b' do
      expect(subject).to parse('b').as(consonant: 'b')
    end

    it 'parses b_<' do
      expect(subject).to parse('b_<').as(consonant: 'b_<')
    end

    it 'parses c' do
      expect(subject).to parse('c').as(consonant: 'c')
    end

    it 'parses d' do
      expect(subject).to parse('d').as(consonant: 'd')
    end

    it 'parses d`' do
      expect(subject).to parse('d`').as(consonant: 'd`')
    end

    it 'parses d_<' do
      expect(subject).to parse('d_<').as(consonant: 'd_<')
    end

    it 'parses f' do
      expect(subject).to parse('f').as(consonant: 'f')
    end

    it 'parses g' do
      expect(subject).to parse('g').as(consonant: 'g')
    end

    it 'parses g_<' do
      expect(subject).to parse('g_<').as(consonant: 'g_<')
    end

    it 'parses h' do
      expect(subject).to parse('h').as(consonant: 'h')
    end

    it 'parses h\\' do
      expect(subject).to parse('h\\').as(consonant: 'h\\')
    end

    it 'parses j' do
      expect(subject).to parse('j').as(consonant: 'j')
    end

    it 'parses j\\' do
      expect(subject).to parse('j\\').as(consonant: 'j\\')
    end

    it 'parses k' do
      expect(subject).to parse('k').as(consonant: 'k')
    end

    it 'parses l' do
      expect(subject).to parse('l').as(consonant: 'l')
    end

    it 'parses l`' do
      expect(subject).to parse('l`').as(consonant: 'l`')
    end

    it 'parses l\\' do
      expect(subject).to parse('l\\').as(consonant: 'l\\')
    end

    it 'parses m' do
      expect(subject).to parse('m').as(consonant: 'm')
    end

    it 'parses n' do
      expect(subject).to parse('n').as(consonant: 'n')
    end

    it 'parses n`' do
      expect(subject).to parse('n`').as(consonant: 'n`')
    end

    it 'parses p' do
      expect(subject).to parse('p').as(consonant: 'p')
    end

    it 'parses p\\' do
      expect(subject).to parse('p\\').as(consonant: 'p\\')
    end

    it 'parses q' do
      expect(subject).to parse('q').as(consonant: 'q')
    end

    it 'parses r' do
      expect(subject).to parse('r').as(consonant: 'r')
    end

    it 'parses r`' do
      expect(subject).to parse('r`').as(consonant: 'r`')
    end

    it 'parses r\\' do
      expect(subject).to parse('r\\').as(consonant: 'r\\')
    end

    it 'parses r\`' do
      expect(subject).to parse('r\`').as(consonant: 'r\`')
    end

    it 'parses s' do
      expect(subject).to parse('s').as(consonant: 's')
    end

    it 'parses s`' do
      expect(subject).to parse('s`').as(consonant: 's`')
    end

    it 'parses s\\' do
      expect(subject).to parse('s\\').as(consonant: 's\\')
    end

    it 'parses t' do
      expect(subject).to parse('t').as(consonant: 't')
    end

    it 'parses t`' do
      expect(subject).to parse('t`').as(consonant: 't`')
    end

    it 'parses v' do
      expect(subject).to parse('v').as(consonant: 'v')
    end

    it 'parses w' do
      expect(subject).to parse('w').as(consonant: 'w')
    end

    it 'parses x' do
      expect(subject).to parse('x').as(consonant: 'x')
    end

    it 'parses x\\' do
      expect(subject).to parse('x\\').as(consonant: 'x\\')
    end

    it 'parses z' do
      expect(subject).to parse('z').as(consonant: 'z')
    end

    it 'parses z`' do
      expect(subject).to parse('z`').as(consonant: 'z`')
    end

    it 'parses z\\' do
      expect(subject).to parse('z\\').as(consonant: 'z\\')
    end

    it 'parses B' do
      expect(subject).to parse('B').as(consonant: 'B')
    end

    it 'parses B\\' do
      expect(subject).to parse('B\\').as(consonant: 'B\\')
    end

    it 'parses C' do
      expect(subject).to parse('C').as(consonant: 'C')
    end

    it 'parses D' do
      expect(subject).to parse('D').as(consonant: 'D')
    end

    it 'parses F' do
      expect(subject).to parse('F').as(consonant: 'F')
    end

    it 'parses G' do
      expect(subject).to parse('G').as(consonant: 'G')
    end

    it 'parses G\_<' do
      expect(subject).to parse('G\_<').as(consonant: 'G\_<')
    end

    it 'parses H' do
      expect(subject).to parse('H').as(consonant: 'H')
    end

    it 'parses H\\' do
      expect(subject).to parse('H\\').as(consonant: 'H\\')
    end

    it 'parses J' do
      expect(subject).to parse('J').as(consonant: 'J')
    end

    it 'parses J\\' do
      expect(subject).to parse('J\\').as(consonant: 'J\\')
    end

    it 'parses J\_<' do
      expect(subject).to parse('J\_<').as(consonant: 'J\_<')
    end

    it 'parses K' do
      expect(subject).to parse('K').as(consonant: 'K')
    end

    it 'parses K\\' do
      expect(subject).to parse('K\\').as(consonant: 'K\\')
    end

    it 'parses L' do
      expect(subject).to parse('L').as(consonant: 'L')
    end

    it 'parses L\\' do
      expect(subject).to parse('L\\').as(consonant: 'L\\')
    end

    it 'parses M\\' do
      expect(subject).to parse('M\\').as(consonant: 'M\\')
    end

    it 'parses N' do
      expect(subject).to parse('N').as(consonant: 'N')
    end

    it 'parses N\\' do
      expect(subject).to parse('N\\').as(consonant: 'N\\')
    end

    it 'parses O\\' do
      expect(subject).to parse('O\\').as(consonant: 'O\\')
    end

    it 'parses P' do
      expect(subject).to parse('P').as(consonant: 'P')
    end

    it 'parses R' do
      expect(subject).to parse('R').as(consonant: 'R')
    end

    it 'parses R\\' do
      expect(subject).to parse('R\\').as(consonant: 'R\\')
    end

    it 'parses S' do
      expect(subject).to parse('S').as(consonant: 'S')
    end

    it 'parses T' do
      expect(subject).to parse('T').as(consonant: 'T')
    end

    it 'parses W' do
      expect(subject).to parse('W').as(consonant: 'W')
    end

    it 'parses X' do
      expect(subject).to parse('X').as(consonant: 'X')
    end

    it 'parses X\\' do
      expect(subject).to parse('X\\').as(consonant: 'X\\')
    end

    it 'parses Z' do
      expect(subject).to parse('Z').as(consonant: 'Z')
    end

    it 'parses 4' do
      expect(subject).to parse('4').as(consonant: '4')
    end

    it 'parses 5' do
      expect(subject).to parse('5').as(consonant: '5')
    end

    it 'parses ?' do
      expect(subject).to parse('?').as(consonant: '?')
    end

    it 'parses ?\\' do
      expect(subject).to parse('?\\').as(consonant: '?\\')
    end

    it 'parses <\\' do
      expect(subject).to parse('<\\').as(consonant: '<\\')
    end

    it 'parses >\\' do
      expect(subject).to parse('>\\').as(consonant: '>\\')
    end

    it 'parses !\\' do
      expect(subject).to parse('!\\').as(consonant: '!\\')
    end

    it 'parses |\\' do
      expect(subject).to parse('|\\').as(consonant: '|\\')
    end

    it 'parses |\|\\' do
      expect(subject).to parse('|\|\\').as(consonant: '|\|\\')
    end

    it 'parses =\\' do
      expect(subject).to parse('=\\').as(consonant: '=\\')
    end
  end
end
