require 'parslet'

module Xsampa
  class Parser < Parslet::Parser
    # rule(:letter) { match('[a-zA-Z]') }
    #
    # rule(:backslash) { str('\\') }
    # rule(:backtick) { str('`') }
    #
    # rule(:other_symbol) do
    #   str('.') | str('"') | str('%') | str('\'') | str(':') | str(':\\') |
    #     str('-') | str('*') | str('/') | str('<') | str('>') | str('^') |
    #     str('!') | str('-\\')
    # end
    #
    # rule(:modifiers) { backslash.maybe >> backtick.maybe }
    #
    # rule(:symbol) do
    #   ((letter >> modifiers) | other_symbol).as(:symbol)
    # end
    #
    # rule(:space) { match('\s') }
    #
    # rule(:word) { symbol.repeat.as(:word) }
    # rule(:words) { (word >> space).repeat }
    # rule(:expression) { words.maybe >> word }
    #
    # root(:expression)
    rule(:alveolar_lateral_click) { str('|\|\\') }
    rule(:retroflex_consonant) { match('[dlnrstz]') >> str('`') }
    rule(:implosive_consonant) { match('[bdg]') >> str('_<') }
    rule(:backslash_consonant) do
      match('[hjlprsvxzBHJKLMNORX?<>!|=]') >> str('\\')
    end
    rule(:retroflex_backtick_consonant) { match('[r]') >> str('\\`') }
    rule(:implosive_backslash_consonant) { match('[GJ]') >> str('\_<') }
    rule(:regular_consonant) do
      match('[bcdfghjklmnpqrstvwxzBCDFGHJKLNPRSTWXZ45?]')
    end

    rule(:consonant) do
      alveolar_lateral_click |
        retroflex_backtick_consonant |
        implosive_backslash_consonant |
        backslash_consonant |
        retroflex_consonant |
        implosive_consonant |
        regular_consonant
    end

    rule(:backslash_vowel) { match('[IU@3]') >> str('\\') }
    rule(:regular_vowel) { match('[aeiouyAEIMOQUVY@{}1236789&]') }

    rule(:vowel) { backslash_vowel | regular_vowel }

    rule(:major_group) { str('||') }
    rule(:downstep) { str('!') }
    rule(:backslash_symbol) { match('[-:]') >> str('\\') }
    rule(:regular_other_symbol) { match('[."%\':-|/*-]') }

    rule(:other_symbol) do
      major_group |
        downstep |
        backslash_symbol |
        regular_other_symbol
    end

    rule(:expression) do
      consonant.as(:consonant) |
        vowel.as(:vowel) |
        other_symbol.as(:other_symbol)
    end

    root(:expression)
  end
end
