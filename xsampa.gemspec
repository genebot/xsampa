# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = 'xsampa'
  s.platform = Gem::Platform::RUBY
  s.version = '0.0.1'

  s.authors = ['Gene Doyel']
  s.email = 'genedoyel@gmail.com'
  s.files = Dir.glob('{lib,spec}/**/*') + [
    'LICENSE', 'Rakefile', 'README.md', 'xsampa.gemspec'
  ]
  s.homepage = 'http://github.com/genebot/xsampa'
  s.license = 'MIT'
  s.require_paths = ['lib']
  s.summary = 'Ruby X-SAMPA to IPA converter'

  s.add_dependency 'parslet'

  s.add_development_dependency 'rake'
  s.add_development_dependency 'rspec'
  s.add_development_dependency 'pry'
end
